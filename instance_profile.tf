resource "aws_iam_role" "instance_role" {
  name = "test_instance_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": ["ec2.amazonaws.com"]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = {
      Name = "test_instance_role"
      Environment = "${var.environment}"
      ManagedBy     = "terraform"
  }
}


resource "aws_iam_instance_profile" "instance_profile" {
  name = "instance_profile"
  role = "${aws_iam_role.instance_role.name}"
}


data "aws_iam_policy_document" "ect_policy_pull_only" {
  statement {
    actions = [
      "ecr:GetAuthorizationToken",
    ]

    resources = ["*"]
  }

  statement {
    actions = [
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer"
    ]

    resources = [
      aws_ecr_repository.ecr_app_py.arn,
    ]
  }

  statement {
    actions = [
      "s3:*",
    ]
    resources = [
      "arn:aws:s3:::${var.codedeploy_s3}",
      "arn:aws:s3:::${var.codedeploy_s3}/*",
    ]
  }

  statement {
    actions = ["codedeploy:*"]
    resources = [
      "arn:aws:codedeploy:${var.region}:${var.aws_account_id}:application:Py_App",
      "arn:aws:codedeploy:${var.region}:${var.aws_account_id}:deploymentgroup:Py_App/*",
      "arn:aws:codedeploy:${var.region}:${var.aws_account_id}:deploymentconfig:*",
    ]
  }
}

resource "aws_iam_role_policy" "instance_policy" {
  name = "instance_policy"
  role = "${aws_iam_role.instance_role.id}"

  policy      = data.aws_iam_policy_document.ect_policy_pull_only.json
}


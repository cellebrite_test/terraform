resource "aws_ecr_repository" "ecr_app_py" {
  name                 = "app_py"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }

  tags = merge(
    {
      Name = "app_py_ecr"
      Group = "test"
    },
    var.extra_tags,
  )
}

resource "aws_ecr_lifecycle_policy" "ecr_app_py_policy" {
  repository = aws_ecr_repository.ecr_app_py.name

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Rule 1",
            "selection": {
                "tagStatus": "tagged",
                "tagPrefixList": ["main"],
                "countType": "imageCountMoreThan",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        },
        {
            "rulePriority": 2,
            "description": "Rule 2",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
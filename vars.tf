variable "centos_ami_id" {
  default = "ami-0358414bac2039369"
}

variable "extra_tags" {
  description = "Extra tags that will be added to all AWS resources that support tags"
  default     = {
    "ManagedBy" = "terraform"
    "Group" = "test"
    "App" = "py_app"
  }
}

variable "instance_type" {
  default = "t3.micro"
}

variable "test_root_volume_size" {
  default = "20"
}

variable "ecr_repo_name" {
  default = ""
}

variable "environment" {
  default = "dev"
}

# tfvars
variable "vpc_id" {}
variable "region" {}
variable "aws_key_pair_main_id" {}
variable "public_subnet_id" {}
variable "internal_cidr_blocks" {}
variable "aws_account_id" {}
variable "download_codedeploy_bucket_name" {}
variable "codedeploy_s3" {}

resource "aws_iam_user" "main" {
  name          = "ecr_test"
  force_destroy = true

  tags = {
    ManagedBy     = "terraform"
    Environment = "${var.environment}"
  }
}

resource "aws_iam_group" "main" {
  name = "ecr_test"
}

resource "aws_iam_group_membership" "main" {
  name  = "ecr-test-group-membership"
  users = ["ecr_test"]
  group = aws_iam_group.main.name
}


data "aws_iam_policy_document" "main" {
  statement {
    actions = [
      "ecr:GetAuthorizationToken",
    ]
    resources = ["*"]
  }

  statement {
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories",
      "ecr:ListImages",
      "ecr:DescribeImages",
      "ecr:BatchGetImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
      "ecr:PutImage",
    ]
    resources = [
      aws_ecr_repository.ecr_app_py.arn,
    ]
  }

  statement {
    actions = [
      "s3:*",
    ]
    resources = [
      "arn:aws:s3:::${var.codedeploy_s3}",
      "arn:aws:s3:::${var.codedeploy_s3}/*",
    ]
  }

  statement {
    actions = ["codedeploy:*"]
    resources = [
      "arn:aws:codedeploy:${var.region}:${var.aws_account_id}:application:Py_App",
      "arn:aws:codedeploy:${var.region}:${var.aws_account_id}:deploymentgroup:Py_App/*",
      "arn:aws:codedeploy:${var.region}:${var.aws_account_id}:deploymentconfig:*",
    ]
  }
}


resource "aws_iam_policy" "main" {
  name        = "ecr-test-policy"
  path        = "/"
  policy      = data.aws_iam_policy_document.main.json

  tags = {
    ManagedBy     = "terraform"
    Environment = "${var.environment}"
  }
}

resource "aws_iam_group_policy_attachment" "main" {
  group      = aws_iam_group.main.name
  policy_arn = aws_iam_policy.main.arn
}
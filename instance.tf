## EC2 instance
resource "aws_instance" "py_app" {
  instance_type = var.instance_type
  ami = var.centos_ami_id
  key_name = var.aws_key_pair_main_id
  vpc_security_group_ids = [aws_security_group.app_py_sg.id]
  availability_zone = "${var.region}a"
  subnet_id = var.public_subnet_id
  iam_instance_profile = "${aws_iam_instance_profile.instance_profile.name}"

  associate_public_ip_address = "true"

  root_block_device {
    volume_size = var.test_root_volume_size
    volume_type = "gp2"
  }

  lifecycle {
    ignore_changes = [
      ami,
      user_data,
    ]
  }

  tags = merge(
    {
      Name = "app_py"
      Environment = "${var.environment}"
    },
    var.extra_tags,
  )

  user_data = <<END_INIT
#!/bin/bash
sudo yum update -y
sudo yum install -y ruby wget docker yum-utils less glibc groff curl unzip
echo "Install Docker"
sudo yum remove -y docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo usermod -a -G docker centos
sudo setfacl --modify user:centos:rw /var/run/docker.sock
sudo systemctl enable docker.service
sudo service docker start
echo "Install CodeDeploy"
wget https://${var.download_codedeploy_bucket_name}.s3.${var.region}.amazonaws.com/latest/install
chmod +x ./install
sudo ./install auto
echo "Install aws cli"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
END_INIT
}


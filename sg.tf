resource "aws_security_group" "app_py_sg" {
  name        = "app_py_sg"
  description = "Allow internal traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "internal all"
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["${var.internal_cidr_blocks}"]
  }

  ingress {
    description = "Public Web"
    from_port   = 0
    to_port     = 8056
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      Name = "app_py_sg"
    },
    var.extra_tags,
  )
}

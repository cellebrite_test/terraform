resource "aws_iam_role" "py_app_role" {
  name = "py-app-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = {
    ManagedBy     = "terraform"
    Environment = "${var.environment}"
  }
}

resource "aws_iam_role_policy_attachment" "AWSCodeDeployRole" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
  role       = aws_iam_role.py_app_role.name
}

resource "aws_codedeploy_app" "py_app" {
  name = "Py_App"

  tags = {
    ManagedBy     = "terraform"
    Environment = "${var.environment}"
  }
}


resource "aws_codedeploy_deployment_group" "py_app" {
  app_name              = aws_codedeploy_app.py_app.name
  deployment_group_name = "py-app-group"
  service_role_arn      = aws_iam_role.py_app_role.arn


  ec2_tag_set {
    ec2_tag_filter {
      key   = "Name"
      type  = "KEY_AND_VALUE"
      value = "app_py"
    }
  }

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  tags = {
    ManagedBy     = "terraform"
    Environment = "${var.environment}"
  }
}
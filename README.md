# terraform
Infra by terraform

_Assuming that the VPC and infrastructure has been created and only an ec2, security group, and CI/CD need to be created._ \
_This is an example that doesn't pretend to be a complete solution and very far from . Created in order to test the test task in the AWS_ \
\

**Add AWS credentials** \
export AWS_ACCESS_KEY_ID=XXXXXXXXXXXXX \
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXX \
or \
mkdir ~/.aws/ \
Add .aws/credentials \
Add .aws/config

# Description

1) instance.tf and instance_profile.tf creates ec2 host with policies needed for AWS codedeploy deployment.
2) sg.tf - security group for the ec2 instance. Allows public access to the web port: 8056
3) iam.tf - User with the permissions required to do all the CI/CD work
4) ecr.tf - creates ECR repository
5) codedeploy.tf - creates AWS CodeDeploy app, group and role needed to deploy ECR image to the ec2
